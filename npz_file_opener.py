import os

import numpy as np

from deepsleep.sleep_stage import print_n_samples_each_class
from deepsleep.utils import get_balance_class_oversample

import re
def _load_npz_list_files( npz_files):
        """Load data and labels from list of npz files."""
        allfiles = os.listdir(npz_files)
        npzfiles = []
        for idx, f in enumerate(allfiles):
            if ".npz" in f:
                npzfiles.append(os.path.join(npz_files, f))
        npzfiles.sort()
        data = []
        labels = []
        fs = None
        for npz_f in npzfiles:
            print("Loading {} ...".format(npz_f))
            tmp_data, tmp_labels, sampling_rate = _load_npz_file(npz_f)
            print(tmp_data)

        return data, labels
def _load_npz_file( npz_file):
    """Load data and labels from a npz file."""
    with np.load(npz_file) as f:
        data = f["x"]
        labels = f["y"]
        sampling_rate = f["fs"]
    return data, labels, sampling_rate
if __name__ == "__main__":
    _load_npz_list_files("data/eeg_fpz_cz")