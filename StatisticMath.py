import numpy as np
import scipy
from matplotlib import pyplot as plt
from scipy.signal import argrelextrema
from scipy import signal
import pywt
#import pyeeg
import math

class StatisticMath:
    def averageAmplitudeChange(total_number_of_sample,epoch):
        out = np.diff(epoch, axis=0)
        sum = np.sum(np.array(out).ravel())/total_number_of_sample    
        return sum;
    def variance(total_number_of_sample,epoch,epoch_30_sec):
        variance = np.var(epoch_30_sec, axis=0)
        return variance;     
    def standard_dev(total_number_of_sample,epoch,epoch_30_sec):
    
        std = np.std(epoch_30_sec, axis=0)
        return std;  
    def square_root(total_number_of_sample,epoch,epoch_30_sec):        
        sum_square_root = np.sum(np.sqrt(epoch_30_sec))
        return sum_square_root;  
    def sum_square_root(total_number_of_sample,epoch,epoch_30_sec):        
        sum_square_root = (np.sum(np.sqrt(np.power(epoch_30_sec,2))))/total_number_of_sample
        return sum_square_root;
    def crest_factor(x):
        return np.max(np.abs(x))/np.sqrt(np.mean(np.square(x)))
    def skew(data):
        return scipy.stats.skew(data, bias=False)
    def kurtosis(data):
        return scipy.stats.kurtosis(data, bias=False)

    def fft(total_number_of_sample,epoch,epoch_30_sec,sampling_rate):
        fft_vals = np.absolute(np.fft.rfft(epoch_30_sec))
        fft_freq = np.fft.rfftfreq(len(epoch_30_sec), 1.0/sampling_rate)
        eeg_bands = {'Delta': (0, 4),
         'Theta': (4, 8),
         'Alpha': (8, 12),
         'Beta': (12, 30),
         'Gamma': (30, 45)}
        eeg_band_fft = dict()
        for band in eeg_bands:  
            freq_ix = np.where((fft_freq >= eeg_bands[band][0]) & 
                            (fft_freq <= eeg_bands[band][1]))[0]
            eeg_band_fft[band] = np.mean(fft_vals[freq_ix])
        # Plot the data (using pandas here cause it's easy)
        import pandas as pd
        import matplotlib.pyplot as plt
        df = pd.DataFrame(columns=['band', 'val'])
        df['band'] = eeg_bands.keys()
        df['val'] = [eeg_band_fft[band] for band in eeg_bands]
        ax = df.plot.bar(x='band', y='val', legend=False)
        ax.set_xlabel("EEG band")
        ax.set_ylabel("Mean band Amplitude")
        plt.show()
        return ;
    def fft_size(total_number_of_sample,epoch,epoch_30_sec,sampling_rate):
        fft_vals = np.absolute(np.fft.rfft(epoch_30_sec))
        fft_freq = np.fft.rfftfreq(len(epoch_30_sec), 1.0/sampling_rate)
        eeg_bands = {'Delta': (0, 4),
         'Theta': (4, 8),
         'Alpha': (8, 12),
         'Beta': (12, 30),
         'Gamma': (30, 45)}
        eeg_band_fft = dict()
        for band in eeg_bands:  
            freq_ix = np.where((fft_freq >= eeg_bands[band][0]) & 
                            (fft_freq <= eeg_bands[band][1]))[0]
            eeg_band_fft[band] = np.mean(fft_vals[freq_ix])
        # Plot the data (using pandas here cause it's easy)
        import pandas as pd
        import matplotlib.pyplot as plt
        df = pd.DataFrame(columns=['band', 'val'])
        df['band'] = eeg_bands.keys()
        df['val'] = [eeg_band_fft[band] for band in eeg_bands]
        key_list = list(df['band'])
        val_list = list(df['val'])
        max_value = np.max(df['val']);
        position = val_list.index(max_value)
        #max(max_value, key=eeg_band_fft)
        #print(eeg_band_fft[max_value])
        bands = dict(sorted(eeg_band_fft.items(), key=lambda item: item[1]))
        #print( position);
        #return list(bands);
        #print(key_list[position])
        return key_list[position];
    def hjorth(total_number_of_sample,epoch,epoch_30_sec):                                             # function for hjorth  (input)
        
        realinput = epoch_30_sec
        hjorth_activity = np.zeros(len(realinput))
        hjorth_mobility = np.zeros(len(realinput))
        hjorth_diffmobility = np.zeros(len(realinput))
        hjorth_complexity = np.zeros(len(realinput))
        diff_input = np.diff(realinput)
        diff_diffinput = np.diff(diff_input)
        hjorth_activity = np.var(epoch_30_sec)
        hjorth_mobility = np.sqrt(np.var(diff_input)/hjorth_activity)
        hjorth_diffmobility = np.sqrt(np.var(diff_diffinput)/np.var(diff_input))
        hjorth_complexity = hjorth_diffmobility/hjorth_mobility
        return [hjorth_activity,hjorth_mobility,hjorth_complexity];
    
    def coeff_var(total_number_of_sample,epoch,epoch_30_sec): #Coefficient_of_Varaition
        mean_i = np.mean(epoch_30_sec) #Saving the mean of array i
        std_i = np.std(epoch_30_sec) #Saving the standard deviation of array i
        output = np.sum(std_i/mean_i) #computing coefficient of variation
        return output
    def first_diff(i):
        b=i
        out = np.zeros(len(b))
        for j in range(len(i)):
            out[j] = b[j-1]-b[j]# Obtaining the 1st Diffs
            j=j+1
            c=out[1:len(out)]
        return c
    def slope_var(_size,singal_epoch,epoch_30_sec):
        b = p = epoch_30_sec #Extracting the data from the 14 channels
        output = np.zeros(len(b)) #Initializing the output array with zeros
        res = np.zeros(len(b)-1)
        
        k = 0; #For counting the current row no.
        x=p
        amp_max = p[argrelextrema(x, np.greater)[0]]#storing maxima value
        t_max = argrelextrema(x, np.greater)[0]#storing time for maxima
        amp_min = p[argrelextrema(x, np.less)[0]]#storing minima value
        t_min = argrelextrema(x, np.less)[0]#storing time for minima value
        t = np.concatenate((t_max,t_min),axis=0) #making a single matrix of all matrix
        t.sort() #sorting according to time

        h=0
        amp = np.zeros(len(t))
        res = np.zeros(len(t)-1)
        for l in range(len(t)):
            amp[l]=p[t[l]]
            
            
        amp_diff = StatisticMath.first_diff(amp)
        t_diff = StatisticMath.first_diff(t)
        for q in range(len(amp_diff)):
            res[q] = amp_diff[q]/t_diff[q] #calculating slope        
        output = np.var(res) 
        k=k+1#counting k
        return output
    def slope_mean(_size,singal_epoch,epoch_30_sec):
        b = p = epoch_30_sec #Extracting the data from the 14 channels
        output = np.zeros(len(b)) #Initializing the output array with zeros
        res = np.zeros(len(b)-1)
        
        k = 0; #For counting the current row no.
        x=p
        amp_max = p[argrelextrema(x, np.greater)[0]]
        t_max = argrelextrema(x, np.greater)[0]
        amp_min = p[argrelextrema(x, np.less)[0]]
        t_min = argrelextrema(x, np.less)[0]
        t = np.concatenate((t_max,t_min),axis=0)
        t.sort()#sort on the basis of time

        h=0
        amp = np.zeros(len(t))
        res = np.zeros(len(t)-1)
        for l in range(len(t)):
            amp[l]=p[t[l]]
            
            
        amp_diff = StatisticMath.first_diff(amp)
        
        t_diff = StatisticMath.first_diff(t)
        
        for q in range(len(amp_diff)):
            res[q] = amp_diff[q]/t_diff[q]         
        output = np.mean(res)
        return output
    def maxPwelch(data_win,Fs):
     
    
        BandF = [0.1, 3, 7, 12, 30]
        PMax = np.zeros([14,(len(BandF)-1)]);
        
        f,Psd = signal.welch(data_win, Fs)
            
        for i in range(len(BandF)-1):
            fr = np.where((f>BandF[i]) & (f<=BandF[i+1]))
            PMax[1,i] = np.max(Psd[fr])
        
        return np.sum(PMax[:,0])/14,np.sum(PMax[:,1])/14,np.sum(PMax[:,2])/14,np.sum(PMax[:,3])/14
    def calc_shannon_entropy(data_set):
        """
            Input : 1 * N vector
            Output: Float with the wavelet entropy of the input vector,
                    rounded to 3 decimal places.
        """
        # probability = [i ** 2 for i in data_set]
        probability = np.square(data_set)
        shannon_entropy = -np.nansum(probability * np.log2(probability))
        return round(shannon_entropy, 3)
    def wavelet_features(epoch):
        cA,cD=pywt.dwt(epoch,'coif1')
        cA_mean = np.mean(cA)
        cA_std = np.std(cA)
        cA_Energy = np.sum(np.square(cA))
        cD_mean = np.mean(cD)		# mean and standard deviation values of coefficents of each channel is stored .
        cD_std = np.std(cD)
        cD_Energy  = np.sum(np.square(cD))
        Entropy_D  = np.sum(np.square(cD) * np.log(np.square(cD)))
        Entropy_A = np.sum(np.square(cD) * np.log(np.square(cD)))
        return [cA_mean,cA_std,cA_Energy,cD_mean,cD_std,cD_Energy,Entropy_D,Entropy_A]
    

    ''' def SpectralEntropy( x ):

        fs = 128
        band = [1,4,8,12,30]
        b = pyeeg.bin_power(x,band,fs)
        resp = pyeeg.spectral_entropy(x,band,fs,Power_Ratio=b)

        resp = [0 if math.isnan(x) else x for x in resp]

        return resp
    def BandPower( x ):
    
        fs = 128
        band = [1,4,8,12,30]

        resp = pyeeg.bin_power(x,band,fs)

        return resp '''

            